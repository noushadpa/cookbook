#
# Cookbook Name:: test_cookbook
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
apt_update 'Update apt cache daily' do
frequency 800_400
action :periodic
end

package 'apache2'

service 'apache2' do
supports :status => true
action [:enable, :start]
end

directory "Create a directory" do
group "root"
mode "0755"
owner "root"
path "/tmp/test"
end

